<?php
/**
 * Created by PhpStorm.
 * User: delbio
 * Date: 22/12/15
 * Time: 14:20
 */

namespace TheDom\ClockBundle\Actions;


class LigthOnAction extends AbstractClockAction
{

    public function execute($args)
    {
        $clock = $args['clock'];
        //System.out.println("LIGHT ON: ");
        echo 'LIGTH ON'.PHP_EOL;
        echo $clock->showTime().PHP_EOL;
    }

}