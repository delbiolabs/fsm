<?php
/**
 * Created by PhpStorm.
 * User: delbio
 * Date: 22/12/15
 * Time: 14:18
 */

namespace TheDom\ClockBundle\Actions;


class UpdateHrAction extends AbstractClockAction
{
    public function execute($args)
    {
        $clock = $args['clock'];

        $clock->setHr($clock->getHr()+1);

        if ($clock->getHr() == 24)
            $clock->setHr(0);

        echo $clock->showTime() . PHP_EOL;
    }

}