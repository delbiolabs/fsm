<?php
/**
 * Created by PhpStorm.
 * User: delbio
 * Date: 22/12/15
 * Time: 14:43
 */

namespace TheDom\ClockBundle\Actions;


use Delbio\FSMBundle\Automata\Action;
use Delbio\FSMBundle\Automata\StateInterface;

abstract class AbstractClockAction extends Action
{

    protected $actionName;

    function __construct(StateInterface $originState, StateInterface $targetState = null, $actionName = null )
    {
        parent::__construct($originState, $targetState);
        if (!is_null($actionName) && is_string($actionName))
            $this->actionName = $actionName;
    }

    public function getName()
    {
        if (!is_null($this->actionName))
            return $this->actionName;
        else
            return parent::getName();
    }

}