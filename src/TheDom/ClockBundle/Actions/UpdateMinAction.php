<?php
/**
 * Created by PhpStorm.
 * User: delbio
 * Date: 22/12/15
 * Time: 14:19
 */

namespace TheDom\ClockBundle\Actions;


use Delbio\FSMBundle\Automata\Action;

class UpdateMinAction extends AbstractClockAction
{
    public function execute($args)
    {
        $clock = $args['clock'];

        $clock->setMin($clock->getMin()+1);

        if ($clock->getMin() == 60)
            $clock->setMin(0);

        echo $clock->showTime() . PHP_EOL;
    }

}