<?php
/**
 * Created by PhpStorm.
 * User: delbio
 * Date: 21/12/15
 * Time: 23:40
 */

namespace TheDom\ClockBundle;


use Delbio\FSMBundle\Automata\Automaton;
use TheDom\ClockBundle\Actions\LigthOnAction;
use TheDom\ClockBundle\Actions\ModeButtonAction;
use TheDom\ClockBundle\Actions\UpdateHrAction;
use TheDom\ClockBundle\Actions\UpdateMinAction;
use TheDom\ClockBundle\States\NormalDisplayState;
use TheDom\ClockBundle\States\UpdatingHrState;
use TheDom\ClockBundle\States\UpdatingMinState;

class Clock extends Automaton
{
    /**
     * @var integer
     */
    protected $hr;
    /**
     * @var integer
     */
    protected $min;

    function __construct()
    {
        parent::__construct();
        $this->init();
    }

    /**
     * Costruisce la definizione della Macchina
     */
    protected function init()
    {
        /**
         * Add NormalDisplay, UpdatingHr, UpdatingMin
         * Set current NormalDisplay
         */
        $n = new NormalDisplayState();
        $uHr = new UpdatingHrState();
        $uMin = new UpdatingMinState();

        $ligthOn = new LigthOnAction($n, $n, 'ChangeButtonAction');
        $a1 = new ModeButtonAction($n, $uHr);
        $n->addAction($a1);
        $n->addAction($ligthOn);

        $updateHr = new UpdateHrAction($uHr, $uHr, 'ChangeButtonAction');
        $a2 = new ModeButtonAction($uHr, $uMin);
        $uHr->addAction($a2);
        $uHr->addAction($updateHr);

        $updateMin = new UpdateMinAction($uMin, $uMin, 'ChangeButtonAction');
        $a3 = new ModeButtonAction($uMin, $n);
        $uMin->addAction($a3);
        $uMin->addAction($updateMin);

        $this->addState($n);
        $this->addState($uHr);
        $this->addState($uMin);
        $this->setBegin($n);
        $this->addEnd($n);
        $this->setCurrentState($n);

    }

    public function modeButton()
    {
        $this->move('ModeButtonAction');
    }

    public function changeButton()
    {
        $this->doAction('ChangeButtonAction', ['clock'=>$this]);
        $this->move('ChangeButtonAction');

    }

    /**
     * @return int
     */
    public function getHr()
    {
        return $this->hr;
    }

    /**
     * @param int $hr
     */
    public function setHr($hr)
    {
        $this->hr = $hr;
    }

    /**
     * @return int
     */
    public function getMin()
    {
        return $this->min;
    }

    /**
     * @param int $min
     */
    public function setMin($min)
    {
        $this->min = $min;
    }

    public function showTime()
    {
        return sprintf('Current time is Hr: %d Min: %d',
            $this->hr,
            $this->min
        );
    }
}