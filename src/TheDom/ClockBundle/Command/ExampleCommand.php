<?php
/**
 * Created by PhpStorm.
 * User: delbio
 * Date: 22/12/15
 * Time: 14:56
 */

namespace TheDom\ClockBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TheDom\ClockBundle\Clock;

class ExampleCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('fsm:clock-execution-example')
            ->setDescription('Esempio di esecuzione di un automa');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $c = new Clock();
        //STATO “VISUALIZZAZIONE NORMALE" -> AZIONE CHANGE -> SI ACCENDE LA LUCE
        $c->changeButton();

        //STATO “MODIFICHE DELLE ORE” -> AZIONE CHANGE -> INCREMENTO DI +1 L’ORA
        $c->modeButton();
        $c->changeButton();

        //STATO “MODIFICA DEI MINUTI” -> AZIONE CHANGE -> INCREMENTO DI +1 I MINUTI
        $c->modeButton();
        $c->changeButton();

        //STATO “VISUALIZZAZIONE NORMALE" -> AZIONE CHANGE -> SI ACCENDE LA LUCE
        $c->modeButton();
        $c->changeButton();
    }

}