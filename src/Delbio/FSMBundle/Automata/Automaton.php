<?php

namespace Delbio\FSMBundle\Automata;

use Doctrine\Common\Collections\ArrayCollection;
use Delbio\FSMBundle\Automata\StateInterface;

class Automaton implements AutomatonInterface
{
    /**
     * @var StateInterface
     * @access protected
     */
    protected $begin;

    /**
     * @var ArrayCollection
     * @access protected
     */
    protected $end;

    /**
     * @var StateInterface
     * @access protected
     */
    protected $state;

    /**
     * @var ArrayCollection
     * @access protected
     */
    protected $states;

    function __construct()
    {
        $this->states = new ArrayCollection();
        $this->end = new ArrayCollection();
    }

    /**
     * @param StateInterface $state
     * @return void
     */
    public function addState(StateInterface $state) { $this->states->set($state->getName(), $state); }

    /**
     * {@inheritdoc}
     * {@link http://php.net/manual/en/reflectionclass.getshortname.php }
     */
    public function getName() { $reflectionClass = new \ReflectionClass($this); return $reflectionClass->getShortName();  }

    /**
     * {@inheritdoc}
     */
    public function getStates() { return $this->states->getValues(); }

    /**
     * {@inheritdoc}
     */
    public function getBegin() { return $this->begin; }

    /**
     * @param StateInterface $begin
     * @throws \RuntimeException
     * @return void
     */
    public function setBegin(StateInterface $begin)
    {
        if (!is_null($this->begin))
            throw new \RuntimeException('begin state already defined');
        if (!in_array($begin, $this->getStates(), true))
            throw new \RuntimeException('begin state not declared in the automaton');
        $this->begin = $begin;
    }

    /**
     * @param StateInterface $end
     * @return void
     * @throws \RuntimeException
     */
    public function addEnd(StateInterface $end)
    {
        if (!in_array($end, $this->getStates(), true))
            throw new \RuntimeException('end state not declared in the automaton');
        $this->end->add($end);
    }

    /**
     * {@inheritdoc}
     */
    public function getEnd() { return $this->end; }

    /**
     * {@inheritdoc}
     */
    public function isFinished() { return $this->end->contains($this->getCurrentState()); }

    /**
     * @param string $name
     * @return StateInterface
     * @throws \InvalidArgumentException if $name is not string
     */
    public function getState($name)
    {
        if (!is_string($name))
            throw new \InvalidArgumentException('$name must be a string');
        return $this->states->get($name);
    }

    /**
     * {@inheritdoc}
     */
    public function getCurrentState() { return $this->state; }

    /**
     * @param string $actionName
     * @return void
     * @throws \InvalidArgumentException if $actionName is not string
     * @throws \RuntimeException current state not selected in the automaton
     */
    public function move($actionName)
    {
        if (!is_string($actionName))
            throw new \InvalidArgumentException('$actionName must be a string');
        $s = $this->getCurrentState();
        if (is_null($s))
            throw new \RuntimeException('current state not selected in the automaton');
        $a = $s->getAction($actionName);
        $s = $a->getTargetState();
        $this->setCurrentState($s);
    }

    /**
     * @param string $actionName
     * @param array $params
     * @return mixed object
     * @throws \InvalidArgumentException if $actionName is not string
     * @throws \RuntimeException current state not selected in the automaton
     */
    public function doAction($actionName, $params = null)
    {
        if (!is_string($actionName))
            throw new \InvalidArgumentException('$actionName must be a string');
        $s = $this->getCurrentState();
        if (is_null($s))
            throw new \RuntimeException('current state not selected in the automaton');
        $a = $s->getAction($actionName);
        return $a->execute($params);
    }

    /**
     * @param StateInterface $state
     * @throws \InvalidArgumentException if $state is null
     * @throws \RuntimeException is automaton is incomplete
     */
    public function setCurrentState(StateInterface $state = null)
    {
        if ($this->end->isEmpty())
            throw new \RuntimeException('incomplete acutomaton: end state(s) not defined');
        if (is_null($this->begin))
            throw new \RuntimeException('incomplete acutomaton: begin state not defined');
        if (is_null($state))
            throw new \InvalidArgumentException('automaton state cannot be null');
        $this->state = $state;
    }

    /**
     * @param string $stateName
     * @throws \InvalidArgumentException if $stateName is not string
     */
    public function setCurrentStateByName($stateName)
    {
        if (!is_string($stateName))
            throw new \InvalidArgumentException('$stateName must be a string');
        $this->setCurrentState(
            $this->getState($stateName)
        );
    }

    public function checkIntegrity()
    {
        //log.info("checking automaton integrity");
        $b = $this->getBegin();
        if (is_null($b))
            throw new \RuntimeException('automaton has no initial state');
        $el = $this->getEnd();
        if (empty($b->getNextActions()) && !$el->contains($b))
            throw new \RuntimeException('initial state '.$b.' is a dead-end (has no outgoing action)');
        if ($el->isEmpty())
            throw new \RuntimeException('automaton has no end state');
        foreach ($el as $e) {
            if (!empty($e->getNextActions()) && $b !== $e)
                throw new \RuntimeException('end state '.$e.' must be a dead-end (must not have outgoing actions)');
        }
        foreach ($this->getStates() as $e) {
            if (empty($e->getNextActions()) && !$el->contains($e))
                throw new \RuntimeException('state '.$e.' is a dead-end (has no outgoing actions)');
        }
        //log.info("automaton integrity looks ok");
    }

    /**
     * @return string
     */
    function __toString()
    {
        $result = $this->getName().': state->actions mapping';
        $result .= ' {';
        foreach ($this->getStates() as $s) {
            foreach ($s->getNextActions() as $a) {
                $result .= $s.'.'.$a.',';
            }
        }
        $result .= '}';
        return $result;
    }
}