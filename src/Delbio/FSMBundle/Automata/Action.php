<?php

namespace Delbio\FSMBundle\Automata;

use Delbio\FSMBundle\Automata\StateInterface;

class Action implements ActionInterface
{
    /**
     * final property, non modificabile
     * @access protected
     * @var StateInterface
     */
    protected $originState;
    /**
     * final property, non modificabile
     * @access protected
     * @var StateInterface
     */
    protected $targetState;

    /**
     * @param StateInterface $originState
     * @param StateInterface $targetState
     * @throws \Exception if originState is not set
     */
    function __construct(StateInterface $originState, StateInterface $targetState = null)
    {
        $this->originState = $originState;
        $this->targetState = is_null($targetState) ? $originState : $targetState;
    }

    /**
     * @param array $args
     * @return mixed object
     */
    public function execute($args)
    {
        //log.debug("null action body -- ignoring");
        return null;
    }

    /**
     * @return StateInterface
     */
    public function getTargetState() { return $this->targetState; }

    /**
     * @return StateInterface
     */
    public function getOriginState() { return $this->originState; }

    /**
     * @return string
     */
    public function getName() { $reflectionClass = new \ReflectionClass($this); return $reflectionClass->getShortName();  }

    /**
     * {@inheritdoc}
     */
    function __toString() { return $this->getName().'() -> '.$this->getTargetState(); }


}