<?php

namespace Delbio\FSMBundle\Automata;

use Doctrine\Common\Collections\ArrayCollection;

interface AutomatonInterface
{
    /**
     * @return string
     */
    public function getName();

    /**
     * @return StateInterface
     */
    public function getBegin();

    /**
     * @return ArrayCollection <StateInterface>
     */
    public function getEnd();

    /**
     * @param string $name
     * @return StateInterface
     */
    public function getState($name);

    /**
     * @return ArrayCollection <StateInterface>
     */
    public function getStates();

    /**
     * @return StateInterface
     */
    public function getCurrentState();

    /**
     * @param string $actionName
     */
    public function move($actionName);

    /**
     * @param string $actionName
     * @param array $params
     * @return mixed object
     */
    public function doAction($actionName, $params);

    /**
     * @param StateInterface $state
     */
    public function setCurrentState(StateInterface $state);

    /**
     * @param string $stateName
     */
    public function setCurrentStateByName($stateName);

    /**
     * @return boolean
     */
    public function isFinished();

    public function checkIntegrity();
}