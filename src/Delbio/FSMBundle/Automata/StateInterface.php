<?php

namespace Delbio\FSMBundle\Automata;

interface StateInterface {
    /**
     * @return string
     */
    public function getName();

    /**
     * @return array <string>
     */
    public function getNextInputs();

    /**
     * @return array <ActionInterface>
     */
    public function getNextActions();

    /**
     * @param string $actionName
     * @return ActionInterface
     */
    public function getAction($actionName);

    /**
     * @param ActionInterface $action
     * @return void
     */
    public function addAction(ActionInterface $action);
}