<?php

namespace Delbio\FSMBundle\Automata;

use Delbio\FSMBundle\Exception\UnsupportedOperationException;
use Doctrine\Common\Collections\ArrayCollection;

class State implements StateInterface
{
    /**
     * @var ArrayCollection
     * @access private
     */
    private $actionMap;

    function __construct()
    {
        $this->actionMap = new ArrayCollection();
    }

    /**
     * {@inheritdoc}
     * {@link http://php.net/manual/en/reflectionclass.getshortname.php }
     */
    public function getName() { $reflectionClass = new \ReflectionClass($this); return $reflectionClass->getShortName();  }

    /**
     * {@inheritdoc}
     */
    public function getNextInputs() { return $this->actionMap->getKeys(); }

    /**
     * {@inheritdoc}
     */
    public function getNextActions() { return $this->actionMap->getValues(); }

    /**
     * @param ActionInterface $action
     * @throws \Exception|\RuntimeException
     */
    public function addAction(ActionInterface $action)
    {
        if (!($action->getOriginState() === $this))
            throw new \Exception("L'origine della azione deve essere questo stato");
        if ($this->actionMap->containsKey($action->getName()))
            throw new \RuntimeException('cannot add action '.$action->getName().' to '.$this.': state already has an action with the same name');
        $this->actionMap->set($action->getName(), $action);
    }

    /**
     * @param string $actionName
     * @return ActionInterface
     * @throws UnsupportedOperationException
     * @throws \InvalidArgumentException if $actionName is not string
     */
    public function getAction($actionName)
    {
        if (!is_string($actionName))
            throw new \InvalidArgumentException('$actionName must be a string');
        $a = $this->actionMap->get($actionName);
        if (is_null($a))
            throw new UnsupportedOperationException('Invalid action '.$actionName.' in state '.$this);
        return $a;
    }

    /**
     * {@inheritdoc}
     */
    function __toString() {
        $reflectionClass = new \ReflectionClass($this);
        $classShotName = $reflectionClass->getShortName();
        return $this->getName().'[class='.$classShotName.']';
    }

}