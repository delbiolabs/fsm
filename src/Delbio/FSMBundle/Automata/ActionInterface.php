<?php

namespace Delbio\FSMBundle\Automata;

interface ActionInterface {

    /**
     * @return string
     */
    public function getName();

    /**
     * @param array $args
     * @return mixed object
     */
    public function execute($args);

    /**
     * @return StateInterface
     */
    public function getTargetState();

    /**
     * @return StateInterface
     */
    public function getOriginState();
}