<?php

namespace Delbio\FSMBundle\Tests;

use Delbio\FSMBundle\Automata\Action;
use Delbio\FSMBundle\Automata\State;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ActionTest extends KernelTestCase
{
    private $container;

    public function setUp()
    {
        self::bootKernel();
        $this->container = static::$kernel->getContainer();
    }

    /**
     * Test Null Constructor
     */
    public function testNullConstructor()
    {
        try{
            $a = new Action(null, null);
        } catch (\Exception $eNull){
            $this->assertTrue(true);
            return;
        }
        $this->fail('An expected exception has not been raised.');
    }

    /**
     * Test Null TargetState Constructor
     */
    public function testNullTargetStateConstructor()
    {
        $s = new State();
        $a = new Action($s, null);
        $this->assertEquals($s, $a->getOriginState());
        $this->assertEquals($s, $a->getTargetState());

        $a1 = new Action($s);
        $this->assertEquals($s, $a1->getOriginState());
        $this->assertEquals($s, $a1->getTargetState());
    }

    /**
     * Test Correct Constructor
     */
    public function testCorrectConstructor()
    {
        $s = new State();
        $a = new Action($s, $s);
    }

    /**
     * Test getOriginState() , getTargetState()
     */
    public function testGetTargetState()
    {
        $originState = new State();
        $targetState = new State();
        $anotherState = new State();

        $a = new Action($originState, $targetState);
        $this->assertEquals($originState,$a->getOriginState());
        $this->assertEquals($targetState,$a->getTargetState());

        //$this->assertNotEquals($anotherState, $a->getOriginState());
        //$this->assertNotEquals($anotherState, $a->getTargetState());

        //$this->assertNotEquals($targetState, $a->getOriginState());
        //$this->assertNotEquals($originState, $a->getTargetState());
    }

    /**
     * Test metodo execute restituisce null
     */
    public function testExecute()
    {
        $s = new State();
        $a = new Action($s);
        $this->assertNull($a->execute(null));
        $this->assertNull($a->execute(['ciao']));
    }

    /**
     * Test getName
     */
    public function testGetName()
    {
        $s = new State();
        $a = new Action($s);
        $this->assertEquals('Action', $a->getName());
    }

    /**
     * Test __toString
     */
    public function testToString()
    {
        $s = new State();
        $a = new Action($s);
        $actionToString = 'Action() -> State[class=State]';
        $this->assertEquals($actionToString, $a);
        $this->assertEquals($actionToString, $a->__toString());
    }
}
