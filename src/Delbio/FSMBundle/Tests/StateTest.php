<?php

namespace Delbio\FSMBundle\Tests;

use Delbio\FSMBundle\Automata\Action;
use Delbio\FSMBundle\Automata\State;
use Delbio\FSMBundle\example\Action1;
use Delbio\FSMBundle\example\Action2;
use Delbio\FSMBundle\example\State1;
use Delbio\FSMBundle\Exception\UnsupportedOperationException;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;


class StateTest extends KernelTestCase
{
    private $container;

    public function setUp()
    {
        self::bootKernel();
        $this->container = static::$kernel->getContainer();
    }

    /**
     * Test __toString()
     */
    public function testToString()
    {
        $s = new State();
        $stateToString = 'State[class=State]';
        $this->assertEquals($stateToString, $s);
        $this->assertEquals($stateToString, $s->__toString());
    }

    /**
     * Test getName
     */
    public function testGetName()
    {
        $s = new State();
        $this->assertEquals('State', $s->getName());
    }

    /**
     * Test Empty Action Map
     */
    public function testEmptyActionMap()
    {
        $s = new State();
        $this->assertCount(0, $s->getNextInputs());
        $this->assertCount(0, $s->getNextActions());
    }

    /**
     * @expectedException        \Exception
     * @expectedExceptionMessage L'origine della azione deve essere questo stato
     */
    public function testAddActionWithAnotherOriginState()
    {
        $s = new State();
        $s1 = new State1();
        $a = new Action($s1);
        $s->addAction($a);
    }

    /**
     * @expectedException        \RuntimeException
     * @expectedExceptionMessage cannot add action Action to State[class=State]: state already has an action with the same name
     */
    public function testDuplicateAddAction()
    {
        $s = new State();
        $a = new Action($s);
        $s->addAction($a);
        $s->addAction($a);
    }

    /**
     * @throws \Exception
     */
    public function testAddSingleAction()
    {
        $s = new State();
        $a = new Action($s);
        $s->addAction($a);
    }

    /**
     * @throws \Exception
     */
    public function testAddMultipleAction()
    {
        $s = new State();
        $a = new Action($s);
        $a1 = new Action1($s);
        $s->addAction($a);
        $s->addAction($a1);
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function stateWithTwoActionDataProvider()
    {
        $s = new State();
        $a = new Action($s);
        $a1 = new Action1($s);
        $s->addAction($a);
        $s->addAction($a1);
        return [ 'state with two action equal' => [ $s, [ $a, $a1 ] ]];
    }

    /**
     * @param State $s
     * @param array $actions
     * @throws UnsupportedOperationException
     * @dataProvider stateWithTwoActionDataProvider
     */
    public function testGetActionByName(State $s, array $actions)
    {
        $a1 = $s->getAction($actions[0]->getName());
        $this->assertEquals($actions[0], $a1);
    }

    /**
     * @expectedException        \InvalidArgumentException
     * @expectedExceptionMessage $actionName must be a string
     */
    public function testGetActionWithNonStringActionName()
    {
        $s = new State();
        $a = new Action($s);
        $s->getAction($a);
    }

    /**
     * @expectedException        \InvalidArgumentException
     * @expectedExceptionMessage $actionName must be a string
     */
    public function testGetActionWithNullActionName()
    {
        $s = new State();
        $s->getAction(null);
    }

    /**
     * @expectedException        \Delbio\FSMBundle\Exception\UnsupportedOperationException
     * @expectedExceptionMessage Invalid action ciao in state State[class=State]
     */
    public function testGetActionWithEmptyActionMap()
    {
        $s = new State();
        $s->getAction('ciao');
    }

    /**
     * @param State $s
     * @param array $actions
     * @dataProvider stateWithTwoActionDataProvider
     */
    public function testGetActionWithActionNotFound(State $s, array $actions)
    {
        $a2 = new Action2($s);
        $expectedActionName = 'Action2';
        $this->assertEquals($expectedActionName,$a2->getName());
        try{
            $s->getAction($a2->getName());
        } catch (UnsupportedOperationException $e){
            $this->assertEquals('Invalid action '.$expectedActionName.' in state State[class=State]', $e->getMessage());
            return;
        }
        $this->fail('An expected exception has not been raised.');
    }

    public function testGetNextActions()
    {
        $this->markTestSkipped("must be revisited. ");
    }

    public function testGetNextInputs()
    {
        $this->markTestSkipped("must be revisited. ");
    }
}
