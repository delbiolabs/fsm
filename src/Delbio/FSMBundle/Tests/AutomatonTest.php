<?php

namespace Delbio\FSMBundle\Tests;

use Delbio\FSMBundle\Automata\Action;
use Delbio\FSMBundle\Automata\Automaton;
use Delbio\FSMBundle\Automata\AutomatonInterface;
use Delbio\FSMBundle\Automata\State;
use Delbio\FSMBundle\example\State1;
use Delbio\FSMBundle\example\State2;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class AutomatonTest extends KernelTestCase
{
    private $container;

    public function setUp()
    {
        self::bootKernel();
        $this->container = static::$kernel->getContainer();
    }

    /**
     * Testa il costruttore della classe Automaton
     */
    public function testEmptyAutomaton()
    {
        $automaton = new Automaton();
        $this->assertCount(0, $automaton->getStates());
        $this->assertCount(0, $automaton->getEnd());
        $this->assertNull($automaton->getBegin());
        $this->assertNull($automaton->getCurrentState());
    }

    public function testAutomatonGetName()
    {
        $automaton = new Automaton();
        $this->assertEquals('Automaton', $automaton->getName());
    }

    public function testAutomatonIsFinishedFalse()
    {
        $automaton = new Automaton();
        $this->assertFalse($automaton->isFinished());
    }

    public function testAutomatonAddNullStateException()
    {
        $automaton = new Automaton();
        try{
            $automaton->addState(null);
        } catch (\Exception $eNull){
            $this->assertTrue(true);
            return;
        }
        $this->fail('An expected exception has not been raised.');
    }

    public function testAddOneEmptyStateToEmptyAutomaton()
    {
        $automaton = new Automaton();
        $s = new State();
        $automaton->addState($s);
        $this->assertCount(1,$automaton->getStates());
    }

    public function testEmptyAutomatonAddMultipleEmptyState()
    {
        $automaton = new Automaton();
        $s = new State();
        $s1 = new State1();
        $s2 = new State2();
        $automaton->addState($s);
        $this->assertCount(1,$automaton->getStates());
        $automaton->addState($s);
        $this->assertCount(1,$automaton->getStates());
        $automaton->addState($s1);
        $this->assertCount(2,$automaton->getStates());
        $automaton->addState($s1);
        $this->assertCount(2,$automaton->getStates());
        $automaton->addState($s2);
        $this->assertCount(3,$automaton->getStates());
        $automaton->addState($s2);
        $this->assertCount(3,$automaton->getStates());
    }

    /**
     * @expectedException        \InvalidArgumentException
     * @expectedExceptionMessage $name must be a string
     */
    public function testGetStateWithNullParameter()
    {
        $automaton = new Automaton();
        $automaton->getState(null);
    }

    /**
     * @expectedException        \InvalidArgumentException
     * @expectedExceptionMessage $name must be a string
     */
    public function testGetStateWithStateParameter()
    {
        $automaton = new Automaton();
        $automaton->getState(new State());
    }

    public function testGetStateWithNotAddedState()
    {
        $automaton = new Automaton();
        $s = new State();
        $automaton->addState($s);
        $s1 = new State1();
        $this->assertNull($automaton->getState($s1->getName()));
        $this->assertNull($automaton->getState('ciao'));
    }

    public function testGetStateWithAddedState()
    {
        $automaton = new Automaton();
        $s = new State();
        $s1 = new State1();
        $s2 = new State2();

        $automaton->addState($s);
        $automaton->addState($s1);
        $automaton->addState($s2);


        $this->assertEquals($s ,$automaton->getState( $s->getName()));
        $this->assertEquals($s1,$automaton->getState($s1->getName()));
        $this->assertEquals($s2,$automaton->getState($s2->getName()));
    }

    /**
     * test setBegin with null param
     */
    public function testSetBeginNull()
    {
        $automaton = new Automaton();
        try{
            $automaton->setBegin(null);
        } catch (\Exception $eNull){
            $this->assertTrue(true);
            return;
        }
        $this->fail('An expected exception has not been raised.');
    }

    public function testSetBeginAndGetBegin()
    {
        $automaton = new Automaton();
        $s = new State();
        $automaton->addState($s);
        $automaton->setBegin($s);
        $this->assertEquals($s, $automaton->getBegin());
    }

    /**
     * @expectedException        \RuntimeException
     * @expectedExceptionMessage begin state already defined
     */
    public function testMultipleExecutionSetBegin()
    {
        $automaton = new Automaton();
        $s = new State();
        $automaton->addState($s);
        $automaton->setBegin($s);
        $s = new State();
        $automaton->setBegin($s);
    }

    /**
     * @expectedException        \RuntimeException
     * @expectedExceptionMessage begin state not declared in the automaton
     */
    public function testSetBeginWithBeginStateNotDeclaredInTheAutomaton()
    {
        $automaton = new Automaton();
        $s = new State();
        $automaton->setBegin($s);
    }

    /**
     * test addEnd with null param
     */
    public function testAddEndNull()
    {
        $automaton = new Automaton();
        try{
            $automaton->addEnd(null);
        } catch (\Exception $eNull){
            $this->assertTrue(true);
            return;
        }
        $this->fail('An expected exception has not been raised.');
    }

    public function testAddEndAndGetEnd()
    {
        $automaton = new Automaton();
        $s = new State();
        $automaton->addState($s);
        $automaton->addEnd($s);
        $this->assertCount(1, $automaton->getEnd());
        $this->assertEquals($s, $automaton->getEnd()[0]);
    }

    /**
     * @expectedException        \RuntimeException
     * @expectedExceptionMessage end state not declared in the automaton
     */
    public function testAddNotDeclaredEndState()
    {
        $automaton = new Automaton();
        $s = new State();
        $automaton->addState($s);
        $automaton->addEnd($s);
        $s1 = new State1();
        $automaton->addEnd($s1);
    }

    /**
     * TODO controllare se sia corretto che posso aggiungere lo  stesso stato piu' volte come stato end
     */
    public function testAddMultipleEqualStatehasEndState()
    {
        $automaton = new Automaton();
        $s = new State();
        $automaton->addState($s);
        $automaton->addEnd($s);
        $s = $automaton->getEnd()[0];
        $automaton->addEnd($s);
        $this->assertCount(2, $automaton->getEnd());
        $this->assertEquals($s, $automaton->getEnd()[0]);
        $this->assertEquals($s, $automaton->getEnd()[1]);
    }

    /**
     * @expectedException        \RuntimeException
     * @expectedExceptionMessage incomplete acutomaton: end state(s) not defined
     */
    public function testSetCurrentStateWithEmptyAutomaton()
    {
        $automaton = new Automaton();
        $s = new State();
        $automaton->setCurrentState($s);
    }

    /**
     * @expectedException           \RuntimeException
     * @expectedExceptionMessage    incomplete acutomaton: begin state not defined
     */
    public function testSetCurrentStateWithNullBeginAutomaton()
    {
        $automaton = new Automaton();
        $s = new State();
        $automaton->addState($s);
        $automaton->addEnd($s);
        $automaton->setCurrentState($s);
    }

    /**
     * Create an Automaton with only one state State
     * @return Automaton
     */
    protected function createAutomatonWithOnlyOneState()
    {
        $automaton = new Automaton();
        $s = new State();
        $automaton->addState($s);
        $automaton->addEnd($s);
        $automaton->setBegin($s);
        return $automaton;
    }

    /**
     * @expectedException           \InvalidArgumentException
     * @expectedExceptionMessage    automaton state cannot be null
     */
    public function testSetCurrentStateWithNullState()
    {
        $automaton = $this->createAutomatonWithOnlyOneState();
        $automaton->setCurrentState(null);
    }

    public function testSetCurrentStateWithNotStateValue()
    {
        $automaton = $this->createAutomatonWithOnlyOneState();
        try{
            $automaton->setCurrentState(true);
        } catch (\Exception $e) {
            $this->assertTrue(true);
            return;
        }
        $this->fail('An expected exception has not been raised.');
    }

    public function testSetCurrentState()
    {
        $automaton = $this->createAutomatonWithOnlyOneState();
        $s = new State();
        $automaton->setCurrentState($s);
        $this->assertEquals($s, $automaton->getCurrentState());
    }

    /**
     * TODO is correct set the current state with not defined state?
     */
    public function testSetCurrentStateWithNotDefinedState()
    {
        $automaton = $this->createAutomatonWithOnlyOneState();
        $s1 = new State1();
        $automaton->setCurrentState($s1);
        $this->assertEquals($s1, $automaton->getCurrentState());
    }

    /**
     * @expectedException           \InvalidArgumentException
     * @expectedExceptionMessage    $stateName must be a string
     */
    public function testSetCurrentStateByNameWithNonStringParameter()
    {
        $automaton = $this->createAutomatonWithOnlyOneState();
        $automaton->setCurrentStateByName(1);
    }

    /**
     * @expectedException           \InvalidArgumentException
     * @expectedExceptionMessage    automaton state cannot be null
     */
    public function testSetCurrentStateByNameWithNotDefinedState()
    {
        $automaton = $this->createAutomatonWithOnlyOneState();
        $s2 = new State2();
        $automaton->setCurrentStateByName($s2->getName());
    }

    public function testSetCurrentStateByName()
    {
        $automaton = $this->createAutomatonWithOnlyOneState();
        $s = new State();
        $automaton->setCurrentStateByName($s->getName());
        $this->assertEquals($s, $automaton->getCurrentState());
    }

    /**
     * Create Automaton with one State and one Action(State,State)
     * @return Automaton
     * @throws \Exception
     */
    protected function createBaseAutomaton()
    {
        // Crea Stati e azioni
        $s = new State();
        $a = new Action($s,$s);
        // associa azioni a stato
        $s->addAction($a);
        // Crea automa, aggiungi stati e stato inizio e stati di fine
        $automaton = new Automaton();
        $automaton->addState($s);
        $automaton->setBegin($s);
        $automaton->addEnd($s);
        return $automaton;
    }

    /**
     * @expectedException           \InvalidArgumentException
     * @expectedExceptionMessage    $actionName must be a string
     */
    public function testDoActionWithNotStringActionName()
    {
        $automaton = $this->createBaseAutomaton();
        $automaton->doAction(1);
    }

    /**
     * @expectedException           \RuntimeException
     * @expextedExceptionMessage    current state not selected in the automaton
     */
    public function testDoActionWithNullCurrentState()
    {
        $automaton = $this->createBaseAutomaton();
        $a = $automaton->getState('State')->getAction('Action');
        $automaton->doAction($a->getName());
    }

    /**
     * @expectedException           \Delbio\FSMBundle\Exception\UnsupportedOperationException
     * @expectedExceptionMessage    Invalid action ciao in state State[class=State]
     */
    public function testDoActionWithUnsupportedAction()
    {
        $automaton = $this->createBaseAutomaton();
        $automaton->setCurrentStateByName('State');
        $automaton->doAction('ciao');
    }

    public function testDoAction()
    {
        $automaton = $this->createBaseAutomaton();
        $automaton->setCurrentStateByName('State');
        $this->assertNull($automaton->doAction('Action'));
    }

    /**
     * @expectedException           \InvalidArgumentException
     * @expectedExceptionMessage    $actionName must be a string
     */
    public function testMoveWithNotStringActionName()
    {
        $automaton = $this->createBaseAutomaton();
        $automaton->move(1);
    }

    /**
     * @expectedException           \RuntimeException
     * @expextedExceptionMessage    current state not selected in the automaton
     */
    public function testMoveWithNullCurrentState()
    {
        $automaton = $this->createBaseAutomaton();
        $a = $automaton->getState('State')->getAction('Action');
        $automaton->move($a->getName());
    }

    /**
     * @expectedException           \Delbio\FSMBundle\Exception\UnsupportedOperationException
     * @expectedExceptionMessage    Invalid action ciao in state State[class=State]
     */
    public function testMoveWithUnsupportedAction()
    {
        $automaton = $this->createBaseAutomaton();
        $automaton->setCurrentStateByName('State');
        $automaton->move('ciao');
    }

    public function testMove()
    {
        $automaton = $this->createBaseAutomaton();
        $automaton->setCurrentStateByName('State');
        $automaton->move('Action');
        $s = $automaton->getState('State');
        $this->assertEquals($s, $automaton->getCurrentState());
    }

    /**
     * Test integrita' automa minimo
     */
    public function testIntegrity()
    {
        // Crea Stati e azioni
        $s = new State();
        $a = new Action($s,$s);
        // associa azioni a stato
        $s->addAction($a);
        // Crea automa, aggiungi stati e stato inizio e stati di fine
        $automaton = new Automaton();
        $automaton->addState($s);
        $automaton->setBegin($s);
        $automaton->addEnd($s);
        $automaton->checkIntegrity();
    }

}
