<?php

namespace Delbio\FSMBundle\Command;


use Delbio\FSMBundle\Automata\Action;
use Delbio\FSMBundle\Automata\Automaton;
use Delbio\FSMBundle\Automata\AutomatonInterface;
use Delbio\FSMBundle\Automata\State;
use Delbio\FSMBundle\example\Action1;
use Delbio\FSMBundle\example\State1;
use Symfony\Bridge\Monolog\Handler\ConsoleHandler;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ExempleCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('fsm:automata-execution-example')
            ->setDescription('Esempio di esecuzione di un automa');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->checkIntegrity($output);
    }

    protected function checkIntegrity(OutputInterface $output)
    {
        // Crea Stati e azioni
        $s = new State();
        $a = new Action($s,$s);

        // associa azioni a stato
        $s->addAction($a);

        // Crea automa, aggiungi stati e stato inizio e stati di fine
        $automaton = new Automaton();
        $automaton->addState($s);
        $automaton->setBegin($s);
        $automaton->addEnd($s);

        //$this->printInfo($automaton, $output);
        $output->writeln($automaton->__toString());
        $automaton->checkIntegrity();
        $output->writeln("Integrity OK");
    }

    public function printInfo(AutomatonInterface $automaton, OutputInterface $output)
    {
        $output->writeln($automaton->getName().': state->actions mapping');
        foreach ($automaton->getStates() as $s) {
            foreach ($s->getNextActions() as $a) {
                $output->writeln('\t'.$s.'.'.$a);
            }
        }
    }

}